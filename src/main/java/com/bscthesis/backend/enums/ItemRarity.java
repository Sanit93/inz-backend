package com.bscthesis.backend.enums;

public enum ItemRarity {
    COMMON,UNCOMMON,RARE,EPIC,LEGENDARY
}
