package com.bscthesis.backend.enums;

public enum ItemSlot {
    CHEST,HEAD,LEGS,FEET,MAIN_HAND,OFF_HAND
}
