package com.bscthesis.backend.enums;

public enum ItemType {
    STR_WEAPON, INT_WEAPON, AGI_WEAPON, ARMOR
}
