package com.bscthesis.backend.dto.response;

import com.bscthesis.backend.persistance.db.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
public class FightResponse {

    private final String enemyName;

    private Optional<Item> drop;

    private Optional<Integer> gold;

    private Optional<Integer> exp;

    private String intro;

    private String outro;

    private List<Turn> turns = new ArrayList<>();

    private int turnCounts;

    public FightResponse(String enemyName) {
        this.enemyName = enemyName;
    }

    public FightResponse(String enemyName, Optional<Item> drop, Optional<Integer> gold) {
        this.enemyName = enemyName;
        this.drop = drop;
        this.gold = gold;
    }

    public void addTurn(Turn turn) {
        this.turnCounts++;
        this.turns.add(turn);
    }

    public void setTurns(List<Turn> turns) {
        this.turnCounts = turns.size();
        this.turns = turns;
    }

    @AllArgsConstructor
    @Getter
    public static class Turn {
        long enemyHealth;
        long characterHealth;
    }
}
