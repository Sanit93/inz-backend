package com.bscthesis.backend.dto.request;

import com.bscthesis.backend.enums.QuestType;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
public class QuestRequest {
    @NotNull
    private QuestType questType;

    @NotBlank
    private String description;

    private int level;

    @NotBlank
    private String enemyName;
}
