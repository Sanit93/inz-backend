package com.bscthesis.backend.dto.request;

import com.bscthesis.backend.enums.ItemRarity;
import com.bscthesis.backend.enums.ItemSlot;
import com.bscthesis.backend.enums.ItemType;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@ToString
public class ItemRequest {
    @NotNull
    @Min(1)
    private Integer level;

    @NotBlank
    private String name;

    @NotNull
    @Min(1)
    private Integer value;

    @NotNull
    @Min(1)
    private Integer itemStat;

    @NotNull
    private ItemSlot itemSlot;

    @NotNull
    private ItemType itemType;

    @NotNull
    private ItemRarity itemRarity;
}
