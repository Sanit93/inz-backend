package com.bscthesis.backend.dto.request;

import com.bscthesis.backend.enums.QuestType;
import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Getter
public class EnemyRequest {

    @NotBlank
    private String name;

    @NotNull
    private Integer level;

    @NotNull
    private Integer health;

    @NotNull
    private Integer damage;

    @NonNull
    private Integer armor;

    @NotNull
    private QuestType questType;
}
