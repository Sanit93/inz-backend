package com.bscthesis.backend.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class CharacterRequest {

    @NotBlank
    private String name;
}
