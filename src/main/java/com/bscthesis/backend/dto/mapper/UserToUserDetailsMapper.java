package com.bscthesis.backend.dto.mapper;

import com.bscthesis.backend.persistance.db.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserToUserDetailsMapper {
    public static UserDetails map(User user) {
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        user.getRoles().forEach(role->grantedAuthorityList.add(new SimpleGrantedAuthority(role)));

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                grantedAuthorityList
        );
    }
}
