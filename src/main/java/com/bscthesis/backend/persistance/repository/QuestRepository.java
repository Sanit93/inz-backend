package com.bscthesis.backend.persistance.repository;

import com.bscthesis.backend.persistance.db.Quest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestRepository extends MongoRepository<Quest, String> {
}
