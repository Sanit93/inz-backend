package com.bscthesis.backend.persistance.repository;

import com.bscthesis.backend.persistance.db.Item;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends MongoRepository<Item, String> {

    Optional<Item> findByName(String name);

    List<Item> findByLevelLessThan(int level);
}
