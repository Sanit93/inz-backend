package com.bscthesis.backend.persistance.db;

import com.bscthesis.backend.enums.QuestType;
import com.bscthesis.backend.model.Enemy;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class Quest {
    @Id
    private String id;

    private int level;
    private Enemy enemy;
    private QuestType questType;
    private String description;
    private int gold;
}
