package com.bscthesis.backend.persistance.db;

import com.bscthesis.backend.model.Character;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Data
@Document
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class User {
    @Id
    private String id;

    @Indexed(unique = true)
    @NonNull
    private String email;

    @NonNull
    private String password;

    @NonNull
    private Character character;

    @NonNull
    private Set<String> roles;
}
