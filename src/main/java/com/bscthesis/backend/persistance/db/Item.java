package com.bscthesis.backend.persistance.db;

import com.bscthesis.backend.enums.ItemRarity;
import com.bscthesis.backend.enums.ItemSlot;
import com.bscthesis.backend.enums.ItemType;
import lombok.*;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Item {
    @Id
    private String id;

    @UniqueElements
    private String name;
    @NonNull
    private Integer level;
    @NonNull
    private Integer itemStat;
    @NonNull
    private ItemSlot itemSlot;
    @NonNull
    private ItemType itemType;
    @NonNull
    private ItemRarity itemRarity;
    @NonNull
    private Integer value;
}
