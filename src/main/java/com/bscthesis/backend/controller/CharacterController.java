package com.bscthesis.backend.controller;

import com.bscthesis.backend.dto.response.FightResponse;
import com.bscthesis.backend.enums.QuestType;
import com.bscthesis.backend.model.Character;
import com.bscthesis.backend.service.CharacterService;
import com.bscthesis.backend.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(value = "/characters",produces = MediaType.APPLICATION_JSON_VALUE)
public class CharacterController {

    @Autowired
    private CharacterService service;

    @GetMapping
    public Character getCharacter(@RequestHeader("Authorization") String bearer){
        String userId = JwtUtils.extractIdFromBearer(bearer);
        return service.getCharacter(userId);
    }

    @GetMapping("equip/{index}")
    public Character equipItem(@RequestHeader("Authorization") String bearer, @PathVariable int index) {
        String userId = JwtUtils.extractIdFromBearer(bearer);
        return service.equipItem(index,userId);
    }

    @GetMapping("quest/{questType}")
    public FightResponse goToQuest(@RequestHeader("Authorization") String bearer, @PathVariable QuestType questType) throws NoSuchAlgorithmException {
        String userId = JwtUtils.extractIdFromBearer(bearer);
        return service.goToQuest(userId, questType);
    }

    @GetMapping("arena")
    public FightResponse goToArena(@RequestHeader("Authorization") String bearer) {
        String userId = JwtUtils.extractIdFromBearer(bearer);
        return service.goToArena(userId);
    }

    @GetMapping("sell/{itemSlot}")
    public Character sellItem(@RequestHeader("Authorization") String bearer, @PathVariable int itemSlot) {
        String userId = JwtUtils.extractIdFromBearer(bearer);
        return service.sellItem(userId, itemSlot);
    }

    @GetMapping("increase/{stat}")
    public Character increaseStat(@RequestHeader("Authorization") String bearer,@PathVariable String stat) {
        String userId = JwtUtils.extractIdFromBearer(bearer);
        return service.increaseStat(userId, stat);
    }
}
