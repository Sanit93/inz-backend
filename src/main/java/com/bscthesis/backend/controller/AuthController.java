package com.bscthesis.backend.controller;

import com.bscthesis.backend.dto.mapper.UserToUserDetailsMapper;
import com.bscthesis.backend.dto.request.LoginRequest;
import com.bscthesis.backend.dto.request.RegisterRequest;
import com.bscthesis.backend.persistance.db.User;
import com.bscthesis.backend.service.UserService;
import com.bscthesis.backend.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody RegisterRequest request) {
        return userService.registerUser(request);
    }

    @PostMapping(value = "/login",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String login(LoginRequest request) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        } catch (BadCredentialsException e) {

            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Incorrect email or password");
        }

        User user = userService.getUserByEmail(request.getEmail());

        return jwtUtils.generateToken(UserToUserDetailsMapper.map(user), user.getId());
    }
}
