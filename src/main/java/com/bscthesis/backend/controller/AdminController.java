package com.bscthesis.backend.controller;

import com.bscthesis.backend.dto.request.ItemRequest;
import com.bscthesis.backend.dto.request.QuestRequest;
import com.bscthesis.backend.persistance.db.Quest;
import com.bscthesis.backend.persistance.db.Item;
import com.bscthesis.backend.service.ItemService;
import com.bscthesis.backend.service.QuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(value = "/admin",produces = MediaType.APPLICATION_JSON_VALUE)
public class AdminController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private QuestService questService;

    @PostMapping("item")
    public Item addItem(@RequestBody ItemRequest request) {
        return itemService.addItem(request);
    }

    @PostMapping("quest")
    @ResponseBody
    public Quest addQuest(@RequestBody QuestRequest request) throws NoSuchAlgorithmException {
        return questService.addQuest(request);
    }
}
