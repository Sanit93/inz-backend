package com.bscthesis.backend.service;

import com.bscthesis.backend.dto.request.QuestRequest;
import com.bscthesis.backend.enums.QuestType;
import com.bscthesis.backend.model.Enemy;
import com.bscthesis.backend.persistance.db.Quest;
import com.bscthesis.backend.persistance.repository.QuestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QuestService {

    private final QuestRepository questRepository;

    private final ItemService itemService;

    public Quest addQuest(QuestRequest request) throws NoSuchAlgorithmException {
        Enemy enemy;
        if (request.getQuestType() == QuestType.QUEST) {
            enemy = Enemy.builder()
                    .questType(QuestType.QUEST)
                    .armor(request.getLevel() * 5)
                    .health(request.getLevel() * 100)
                    .damage(request.getLevel() * 3)
                    .name(request.getEnemyName())
                    .build();
        } else {
            enemy = Enemy.builder()
                    .questType(QuestType.DUNGEON)
                    .armor(request.getLevel() * 7)
                    .health(request.getLevel() * 150)
                    .damage(request.getLevel() * 5)
                    .name(request.getEnemyName())
                    .build();
        }

        int gold = request.getLevel() * 5;

        Quest quest = Quest.builder()
                .questType(request.getQuestType())
                .gold(gold)
                .level(request.getLevel())
                .enemy(enemy)
                .description(request.getDescription())
                .build();

        return questRepository.save(quest);
    }

    public Quest getRandomQuest(int level, QuestType questType) throws NoSuchAlgorithmException {
        List<Quest> quests = questRepository.findAll().stream()
                .filter(q -> q.getLevel() >= level - 1 || q.getLevel() <= level + 1)
                .filter(q -> q.getQuestType() == questType)
                .collect(Collectors.toList());

        Random rand = SecureRandom.getInstanceStrong();
        return quests.get(rand.nextInt(quests.size()));
    }
}
