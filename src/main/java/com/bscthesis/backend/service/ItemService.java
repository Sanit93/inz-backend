package com.bscthesis.backend.service;

import com.bscthesis.backend.dto.request.ItemRequest;
import com.bscthesis.backend.enums.ItemRarity;
import com.bscthesis.backend.enums.ItemSlot;
import com.bscthesis.backend.enums.ItemType;
import com.bscthesis.backend.enums.QuestType;
import com.bscthesis.backend.persistance.db.Item;
import com.bscthesis.backend.persistance.repository.ItemRepository;
import com.bscthesis.backend.utils.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public Item addItem(ItemRequest request) {
        if (itemRepository.findByName(request.getName()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "item with that name already exists");
        }
        if (request.getItemType().name().contains("WEAPON") && request.getItemSlot() != ItemSlot.MAIN_HAND) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ItemSlot and ItemType are incorrect");
        }

        if (request.getItemType() == ItemType.ARMOR && request.getItemSlot() == ItemSlot.MAIN_HAND) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ItemSlot and ItemType are incorrect");
        }

        Item item = Item.builder()
                .itemRarity(request.getItemRarity())
                .itemSlot(request.getItemSlot())
                .itemType(request.getItemType())
                .itemStat(request.getItemStat())
                .level(request.getLevel())
                .value(request.getValue())
                .name(request.getName())
                .build();
        return itemRepository.save(item);
    }

    public Item getRandomItemByLevel(int level, QuestType questType) throws NoSuchAlgorithmException {
        List<Item> items = itemRepository.findByLevelLessThan(level+1);

        if(questType==QuestType.QUEST) {
            items = items.stream()
                    .filter(i -> i.getItemRarity() == ItemRarity.COMMON || i.getItemRarity() == ItemRarity.UNCOMMON)
                    .collect(Collectors.toList());
        } else {
            items = items.stream()
                    .filter(i -> i.getItemRarity() == ItemRarity.RARE || i.getItemRarity() == ItemRarity.LEGENDARY)
                    .collect(Collectors.toList());
        }

        return items.get(new Random().getRandomInt(items.size()));
    }
}
