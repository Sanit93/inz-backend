package com.bscthesis.backend.service;

import com.bscthesis.backend.dto.response.FightResponse;
import com.bscthesis.backend.enums.QuestType;
import com.bscthesis.backend.model.Character;
import com.bscthesis.backend.model.Enemy;
import com.bscthesis.backend.persistance.db.Item;
import com.bscthesis.backend.persistance.db.Quest;
import com.bscthesis.backend.persistance.db.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CharacterService {

    private final UserService userService;

    private final QuestService questService;

    private final ItemService itemService;

    public Character getCharacter(String userId) {
        return userService.getUserById(userId).getCharacter();
    }

    public Character increaseStat(String userId, String stat) {
        Character character = getCharacter(userId);
        if (character.getGold() < 10) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "You have not enough amount of gold");
        }
        switch (stat) {
            case "str":
                character.increaseStr();
                break;
            case "agi":
                character.increaseAgi();
                break;
            case "int":
                character.increaseInt();
                break;
            case "sta":
                character.increaseSta();
                break;
            case "luc":
                character.increaseLuck();
                break;
            default:
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST,
                        "wrong stat, please use one of \"str\",\"agi\",\"int\",\"sta\",\"luc\""
                );
        }
        return saveCharacter(userId, character);
    }

    public Character sellItem(String userId, int itemSlot) {
        Character character = getCharacter(userId);
        character.sellItem(itemSlot);
        return saveCharacter(userId, character);
    }

    public Character equipItem(int index, String userId) {
        User user = userService.getUserById(userId);
        Character character = user.getCharacter();
        List<Item> characterItems = character.getItems();
        character.equip(characterItems.get(index));
        if (characterItems.size() <= 1) {
            characterItems.clear();
        } else {
            characterItems.remove(index);
        }
        character.setItems(characterItems);
        user.setCharacter(character);
        return userService.saveUser(user).getCharacter();
    }

    public FightResponse goToQuest(String userId, QuestType questType) throws NoSuchAlgorithmException {
        User user = userService.getUserById(userId);
        Character character = user.getCharacter();
        Quest quest = questService.getRandomQuest(character.getLevel(), questType);

        Enemy enemy = quest.getEnemy();
        Integer gold = null;
        Integer exp = null;
        Item item = itemService.getRandomItemByLevel(quest.getLevel(), questType);

        List<FightResponse.Turn> turns = fightEnemy(character, enemy);
        FightResponse fightResponse = new FightResponse(enemy.getName());
        fightResponse.setIntro("Zatakowałeś przeciwnika " + enemy.getName() + " na Arenie");
        fightResponse.setTurns(turns);

        if (youWon(turns)) {
            gold = quest.getGold();
            exp = quest.getLevel() * 10;

            fightResponse.setGold(Optional.of(gold));
            fightResponse.setExp(Optional.of(exp));

            if (item != null) {
                fightResponse.setDrop(Optional.of(item));
                character.addItem(item);
            }
            character.addExperience(exp);
            character.addGold(gold);
            user.setCharacter(character);
            userService.saveUser(user);
            fightResponse.setOutro("Zwyciężyłeś");
            return fightResponse;
        }
        fightResponse.setOutro("Przegrałeś");
        return fightResponse;
    }

    public FightResponse goToArena(String userId) {
        User user = userService.getUserById(userId);
        Character character = user.getCharacter();
        Character enemy = getRandomCharacterToPVP(userId, character.getLevel());

        Integer gold = null;
        Integer exp = null;
        List<FightResponse.Turn> turns = fightEnemy(character, enemy);
        FightResponse fightResponse = new FightResponse(enemy.getName());
        fightResponse.setIntro("Zatakowałeś przeciwnika " + enemy.getName() + " na Arenie");
        fightResponse.setTurns(turns);


        if (youWon(turns)) {
            gold = Math.toIntExact(Math.round(enemy.getGold() * 0.1));
            exp = enemy.getLevel() * 10;

            fightResponse.setGold(Optional.of(gold));
            fightResponse.setExp(Optional.of(exp));

            character.addExperience(exp);
            character.addGold(gold);

            user.setCharacter(character);
            userService.saveUser(user);
            fightResponse.setOutro("Zwyciężyłeś");
            return fightResponse;
        }
        fightResponse.setOutro("Przegrałeś");
        return fightResponse;
    }

    private Character saveCharacter(String userId, Character character) {
        User user = userService.getUserById(userId);
        user.setCharacter(character);
        return userService.saveUser(user).getCharacter();
    }

    private List<FightResponse.Turn> fightEnemy(Character character, Character enemy) {
        long characterHealth = character.getHealth();
        long enemyHealth = enemy.getHealth();
        List<FightResponse.Turn> turns = new ArrayList<>();
        turns.add(new FightResponse.Turn(enemyHealth, characterHealth));
        if (character.getLuck() > enemy.getLuck()) {
            while (enemyHealth > 0) {
                if (characterHealth > 0) {
                    enemyHealth -= calculateDmg(character.getDamage(), enemy.getArmor());
                    characterHealth -= calculateDmg(enemy.getDamage(), character.getArmor());
                    turns.add(new FightResponse.Turn(enemyHealth, characterHealth));
                } else {
                    return turns;
                }
            }
        } else {
            while (characterHealth > 0) {
                if (enemyHealth > 0) {
                    characterHealth -= calculateDmg(enemy.getDamage(), character.getArmor());
                    enemyHealth -= calculateDmg(character.getDamage(), enemy.getArmor());
                    turns.add(new FightResponse.Turn(enemyHealth, characterHealth));
                } else {
                    return turns;
                }
            }
        }
        return turns;
    }

    private List<FightResponse.Turn> fightEnemy(Character character, Enemy enemy) {
        long characterHealth = character.getHealth();
        long enemyHealth = enemy.getHealth();
        List<FightResponse.Turn> turns = new ArrayList<>();
        turns.add(new FightResponse.Turn(enemyHealth, characterHealth));

        while (characterHealth > 0) {
            if (enemyHealth > 0) {
                enemyHealth -= calculateDmg(character.getDamage(), enemy.getArmor());
                characterHealth -= calculateDmg(enemy.getDamage(), character.getArmor());
                turns.add(new FightResponse.Turn(enemyHealth, characterHealth));
            } else {
                return turns;
            }
        }
        return turns;
    }

    private int calculateDmg(int sourceDmg, int targetArmor) {
        if (targetArmor > sourceDmg) {
            return sourceDmg;
        }
        if (targetArmor == sourceDmg) {
            return 1;
        }
        return sourceDmg - targetArmor;
    }

    private Character getRandomCharacterToPVP(String id, int level) {
        List<User> allUsers = userService.getAllUsers();
        allUsers = allUsers.stream().filter(u -> !u.getId().equals(id)).collect(Collectors.toList());

        List<Character> characters = new ArrayList<>();
        allUsers.forEach(user -> characters.add(user.getCharacter()));

        return characters.stream()
                .filter(c -> c.getLevel() == level)
                .findAny().orElse(createBot(level));
    }

    private Character createBot(int level) {
        int statPoint = level + 10;

        return Character.builder()
                .armor(level * 10)
                .damage(level * 15)
                .agility(statPoint)
                .stamina(statPoint)
                .strength(statPoint)
                .intelligence(statPoint)
                .luck(statPoint)
                .gold(level * 5)
                .name("Bot" + level)
                .build();
    }

    private boolean youWon(List<FightResponse.Turn> turns) {
        return turns.get(turns.size() - 1).getCharacterHealth() > 0;
    }
}
