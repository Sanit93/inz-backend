package com.bscthesis.backend.utils;

import lombok.Getter;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Getter
public class Random {

    private SecureRandom rand;

    public Random() throws NoSuchAlgorithmException {
        rand = SecureRandom.getInstanceStrong();
    }

    public int getRandomInt(int bound){
        return rand.nextInt(bound);
    }
}
