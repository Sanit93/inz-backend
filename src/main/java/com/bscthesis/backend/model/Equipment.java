package com.bscthesis.backend.model;

import com.bscthesis.backend.persistance.db.Item;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Equipment {
    private Item chest;
    private Item head;
    private Item legs;
    private Item feet;
    private Item mainHand;
    private Item offHand;

    public void setItem(Item item) {
        switch (item.getItemSlot()){
            case MAIN_HAND:
                setMainHand(item);
                break;
            case OFF_HAND:
                setOffHand(item);
                break;
            case CHEST:
                setChest(item);
                break;
            case LEGS:
                setLegs(item);
                break;
            case HEAD:
                setHead(item);
                break;
            case FEET:
                setFeet(item);
                break;
        }
    }
}
