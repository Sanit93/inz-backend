package com.bscthesis.backend.model;

import com.bscthesis.backend.enums.QuestType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Enemy {

    private String name;

    private int health;

    private int damage;

    private int armor;

    private QuestType questType;
}
