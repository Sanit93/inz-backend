package com.bscthesis.backend.model;

import com.bscthesis.backend.persistance.db.Item;
import lombok.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class Character {

    @NonNull
    @Indexed(unique = true)
    private String name;

    @Builder.Default
    private long health = 100;
    @Builder.Default
    private long exp = 0;

    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int level = 1;
    @Builder.Default
    private int damage = 10;
    @Builder.Default
    private int armor = 0;

    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int strength = 10;
    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int agility = 10;
    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int intelligence = 10;
    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int stamina = 10;
    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int luck = 10;

    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int experienceToLevel = 100;

    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    private int gold = 0;

    @Builder.Default
    private Equipment equipment = new Equipment();
    @Builder.Default
    private List<Item> items = new ArrayList<>();

    public void addExperience(int value) {
        exp += value;
        increaseLevel();
    }

    public void increaseStr() {
        buyStat();
        this.strength += 1;
        setDamage();
    }

    public void increaseAgi() {
        buyStat();
        this.agility += 1;
        setDamage();
    }

    public void increaseInt() {
        buyStat();
        this.intelligence += 1;
        setDamage();
    }

    public void increaseSta() {
        buyStat();
        this.stamina += 1;
        setHealth();
    }

    public void increaseLuck() {
        buyStat();
        this.luck += 1;
    }

    public void addItem(Item item) {
        items.removeAll(Collections.singleton(null));
        if (items.size() > 10) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "U reached limit of items please sell something");
        }
        this.items.add(item);
    }

    public void sellItem(int index) {
        addGold(items.get(index).getValue());
        items.remove(index);
        items.removeAll(Collections.singleton(null));
    }

    public void addGold(int gold) {
        this.gold += gold;
    }

    public void equip(Item item) {
        switch (item.getItemSlot()) {
            case FEET:
                equipItem(item, equipment.getFeet());
                setArmor();
                break;
            case HEAD:
                equipItem(item, equipment.getHead());
                setArmor();
                break;
            case LEGS:
                equipItem(item, equipment.getLegs());
                setArmor();
                break;
            case CHEST:
                equipItem(item, equipment.getChest());
                setArmor();
                break;
            case OFF_HAND:
                equipItem(item, equipment.getOffHand());
                setArmor();
                break;
            case MAIN_HAND:
                equipItem(item, equipment.getMainHand());
                setDamage();
                break;
        }
    }

    private void equipItem(Item item, Item equippedItem) {
        if (equippedItem == null) {
            equipment.setItem(item);
            items.remove(item);
        } else {
            items.add(equippedItem);
            equipment.setItem(item);
        }
    }

    private void increaseLevel() {
        experienceToLevel = level * 100;
        if (exp >= experienceToLevel) {
            level++;
            exp = exp - experienceToLevel;
            setHealth();
            setDamage();
        }
    }

    private void buyStat() {
        this.gold = gold - 10;
    }

    private void setDamage() {
        this.damage = 0;
        if (equipment.getMainHand() != null) {
            switch (equipment.getMainHand().getItemType()) {
                case INT_WEAPON:
                    this.damage = getIntelligence() + level + equipment.getMainHand().getItemStat();
                    break;
                case AGI_WEAPON:
                    this.damage = getAgility() + level + equipment.getMainHand().getItemStat();
                    break;
                case STR_WEAPON:
                    this.damage = getStrength() + level + equipment.getMainHand().getItemStat();
                    break;
                default:
                    break;
            }
        } else {
            this.damage = level + getStrength();
        }
    }

    private void setArmor() {
        int tempArmor = 0;
        if (equipment.getOffHand() != null) {
            tempArmor += equipment.getOffHand().getItemStat();
        }
        if (equipment.getLegs() != null) {
            tempArmor += equipment.getLegs().getItemStat();
        }
        if (equipment.getHead() != null) {
            tempArmor += equipment.getHead().getItemStat();
        }
        if (equipment.getFeet() != null) {
            tempArmor += equipment.getFeet().getItemStat();
        }
        if (equipment.getChest() != null) {
            tempArmor += equipment.getChest().getItemStat();
        }
        armor = tempArmor;
    }

    private void setHealth() {
        this.health = Math.round(health * (0.1 * level) + (stamina * 10));
    }
}
