package com.bscthesis.backend.dto.response;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FightResponseTest {

    @Test
    @DisplayName("increaseTurnsCountWhenSetTurns")
    void setTurns() {
        //given
        FightResponse fightResponse = new FightResponse("Enemy name");
        List<FightResponse.Turn> turnList = List.of(
                new FightResponse.Turn(20, 30),
                new FightResponse.Turn(10, 20),
                new FightResponse.Turn(5, 10),
                new FightResponse.Turn(-2, 5)
        );

        //when
        fightResponse.setTurns(turnList);

        //then
        assertEquals(4, fightResponse.getTurnCounts());
    }

    @Test
    @DisplayName("increaseTurnsCountWhenAddTurn")
    void addTurn() {
        //given
        FightResponse fightResponse = new FightResponse("Enemy name");
        FightResponse.Turn turn = new FightResponse.Turn(20, 30);

        //when
        fightResponse.addTurn(turn);

        //then
        assertEquals(1, fightResponse.getTurnCounts());
    }
}